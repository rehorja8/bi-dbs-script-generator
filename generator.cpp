#include <exception>
#include <climits>
#include <list>
#include <functional>
#include <iostream>
#include <cstdlib>
#include <vector>
#include <string>
#include <sstream>
#include <fstream>
#include <iomanip>
using namespace std;

bool RESET_SEQ = false;
#define VARCHAR_MAX 64

string quote(const string& str)
{
    return "'" + str + "'";
}

// class sequence
// {
// public:
//     size_t m_Idx;
//     sequence(size_t start = 0)
//     : m_Idx(start)
//     {}

//     string operator()()
//     {
//         ostringstream stream;
//         stream << m_Idx;
//         m_Idx++;
//         return stream.str();
//     }
// };


void load(vector<string>& vec, const string& file_name)
{
    ifstream file(file_name);
    string line;
    while (getline(file, line))
    {
        vec.push_back(line);
    }
}

string random_from(const vector<string>& vec)
{
    return vec[rand() % vec.size()];
}


class Lists
{
public:
    Lists()
    {}
    vector<string>alien_races = { "Vorgonator", "Zoraxian", "Krystallite", "Xalaxor", "Dracorian", "Synthetix", "Zargonian", "Celestix", "Arcturian", "Tyriax", "Cygnian", "Zephyrian", "Ophidian", "Fendarian", "Krynnith", "Nebulonix", "Vindorian", "Xandorian", "Thorgonik", "Nalorien", "Uzorath", "Zylynthia", "Gromaxion", "Quiraktar", "Zoragon", "Krystallor", "Xalaxian", "Synthetorian", "Zargonix", "Celestorian", "Arcturix", "Cygnorian", "Zephyrax", "Ophixian", "Fendarix", "Krynnor", "Vorgonar", "Zoraxor", "Krystallik", "Xalaxorion", "Dracorix", "Synthetor", "Zargonar", "Celestor", "Arcturin", "Tyrix", "Cygnix", "Zephyrion", "Ophidianix", "Fendarik", "Krynnik", "Vindorianix", "Xandorix", "Thorgonix", "Nalorian", "Uzorax", "Zylynthian", "Gromaxor", "Quirakor", "Zoragor", "Krystallikor", "Xalaxorianix", "Dracorin", "Synthetorianix", "Zargonixian", "Celestorianix", "Arcturixian", "Tyrixian", "Cygnikor", "Zephyrionix", "Ophidianor", "Fendarion", "Krynnorian", "Vorgonatorix", "Zoraxianix", "Krystalliteix", "Xalaxorianor", "Dracorianix", "Synthetixor", "Zargonianix", "Celestixian", "Arcturianix", "Tyriaxor", "Cygnor", "Zephyrik", "Ophix", "Fendarixian", "Krynnorianix", "Vindorik", "Xandorin", "Thorgonian", "Nalorix", "Uzorix", "Zylynthor", "Gromaxorian", "Quirakorix", "Zoragorian", "Krystallorix", "Xalaxorik", "Dracorax" };
    vector<string>alien_names = { "Zyraxithecrixixadarklynthion", "Vythoraxystomirixululorintha", "Xalorithyndraxalthazorynthion", "Uzuthoraxyrithalixynthiondir", "Yzythorixyndraxixurithalzorin", "Vorthorixalixystorinthezorin", "Wrynnorixyradaxysulathorintha", "Zalorynthiadaxynululorinthia", "Xalorithythorixalzorynxorinth", "Vyraxorixyndraxisythorinthion", "Uzorithyndraxirixalzorinthia", "Zyraxorixalixythorinthiathor", "Vythorixalzorinthyndraxintha", "Xalorithorixyndraxisulathor", "Uzuthorixyndraxalthazorynth", "Yzythorixyradaxysulazorynth", "Vorthorixyradaxirixalzorynx", "Wrynnorixyndraxystorinthaion", "Zalorynthixululorinthyndraxi", "Xalorithyndraxirixululorin", "Vyraxorixululorinthyndraxix", "Uzorithixyradaxirixalzorin", "Zyraxorixyndraxalthazorynxor", "Vythorixalzorinthiadaxysulin", "Xalorithyndraxystorinthiath", "Uzuthorixalixythorinthiondir", "Yzythorixalzorinthiadxirintha", "Vorthorixululorinthiaxalzoryn", "Wrynnorixyndraxixurithalzorin", "Zalorynthiadaxysulathorintha", "Xalorithythorixyndraxalthaz", "Vyraxorixyndraxirixalzorynth", "Uzorithyndraxixululorinthion", "Zyraxorixyradaxysulazorynthi", "Vythorixyndraxalthazorynxorin", "Xalorithyndraxirixululorinth", "Uzuthorixalzorinthyndraxirix", "Yzythorixalzorynthiadaxirixal", "Vorthorixyndraxirixalzorinthy", "Wrynnorixyradaxysulathorinxor", "Zalorynthiadaxysulazorynxorin", "Xalorithythorixyndraxirixal", "Vyraxorixalzorinthiadaxysulin", "Uzorithyndraxixurithalzorynx", "Zyraxorixalixythorinthezorin", "Vythorixalzorynxorinthiadaxys", "Xalorithyndraxystorin", "Vhobolgrothianthorax", "Juzhargotrothimakreth", "Gaxarvithixielarius", "Krythaxorvelesioth", "Vrulgoranthizialyth", "Zhraxadorthianvexal", "Qarulithvandralyx", "Fhivokalxithragar", "Mradrokariantharix", "Thyraxathalirionth", "Szyxarthogalithon", "Gortharaxalithykus", "Xyvendarthozielith", "Vekalorathylithion", "Krygokarixalithion", "Fhulgralithyvorath", "Zyrnathogalixial", "Lyrthixarvaniolus", "Xorvithakalithron", "Tharaxoltharianth", "Szyvaraxithyvex", "Gaxenthrothilakus", "Vrulanthoraxolus", "Zhraxalithorvian", "Qakryxalithyzar", "Fhivogalithothrax", "Mrakarithalixion", "Thyxarxendrothion", "Szyxoltharianthor", "Gorthaxarvithior", "Xyvenzhraxolthar", "Vekalorathilakus", "Krygokarixendrax", "Fhulgralithizian", "Zyrnathogalithral", "Lyrthixarvithorv", "Xorvithakalithoth", "Tharaxolthariolus", "Szyvaraxithygok", "Gaxenthrothirion" };
    vector<string>car_names = { "Velocity Viper", "Hyperion Hurricane", "Warp Wraith", "Nova Nighthawk", "Quantum Quicksilver", "Eclipse Enforcer", "Nebula Nitro", "Starstream Stingray", "Galactic GT", "Cosmic Cobra", "Meteor Maverick", "Aurora Auto", "Skyline Sprinter", "Thunder Turbo", "Interceptor Indy", "Nebula Nascar", "Phantom Ferrari", "Velocity Vette", "Cruiser Corvette", "Infinity Intrepid", "Sky Sprint", "Hyperdrive Hellcat", "Comet Challenger", "Solar System Supra", "Interstellar Impala", "Space Shuttle Shelby", "Warp Wing", "Warpdrive Viper", "Pulsar Porsche", "Starship Stingray", "Galaxy GT-R", "Velocity Valkyrie", "Quantum Quasar", "Supernova Subaru", "Blackhole Bugatti", "Solar Sprint", "Meteor Mustang", "Nebula Nissan", "Rocket Racer", "Starship Subaru", "Velocity Veneno", "Hyperion Hennessey", "Warp Wiesmann", "Nova Noble", "Quantum Q8", "Eclipse Evora", "Nebula Nissan GT-R", "Starstream S2000", "Galactic Gallardo", "Cosmic Carrera", "Meteor Maserati", "Aurora Aventador", "Skyline Skyrocket", "Thunderbird Thunderbolt", "Interceptor Intermeccanica", "Nebula Nomad", "Phantom Plymouth", "Velocity Vanquish", "Cruiser Cuda", "Infinity Iroc", "Hyperdrive Hellfire", "Comet Camaro", "Solar System Skyline", "Interstellar Integra", "Space Shuttle Saturn", "Warpdrive Wagon", "Pulsar Pantera", "Starship Shelby Cobra", "Galaxy GTO", "Velocity Vantage", "Quantum Q7", "Supernova Stinger", "Blackhole Bentley", "Solar Scirocco", "Meteor Murcielago", "Nebula Nova", "Rocket Roadster", "Starship Supra", "Velocity Venom", "Hyperion Huracan", "Warp Wraith GT", "Nova NSX", "Quantum Q5", "Eclipse Elise", "Nebula Navara", "Starstream Solstice", "Galactic GTI", "Cosmic Continental", "Meteor Morgan", "Aurora Acura", "Skyline Skyhawk", "Thunder Truck", "Interceptor Infinity", "Nebula Neon", "Phantom Pontiac", "Velocity Vanagon", "Cruiser Cobalt", "Infinity Insight", "Hyperdrive Hummer", "Comet Corvette Stingray" };
    string get_alien_race()
    {
        return alien_races[rand() % alien_races.size()];
    }
    string get_alien_name()
    {
        return alien_names[rand() % alien_names.size()];
    }

};

Lists L;


string alien_name()
{
    const int MAX_WORDS = 3;
    string result = "";
    int len = rand() % MAX_WORDS + 1;
    for (int i = 0; i < len; i++)
    {
        string another_name = L.get_alien_name();
        if (another_name.size() + result.size() >= VARCHAR_MAX)
            break;

        if (i != 0)
            result += " ";
        result += another_name;
    }
    return quote(result);
}

string alien_race()
{
    return quote(L.get_alien_race());
}

string car_name()
{
    return quote(L.car_names[rand() % L.car_names.size()]);
}

string boolean()
{
    return quote(rand() % 2 ? "t" : "f");
}

class const_string
{
public:
    string m_Str;
    const_string(string str): m_Str(str) {}
    string operator()()
    {
        return m_Str;
    }
};

class from_file
{
public:

    vector<string> m_Data;
    bool m_Uniq;
    size_t m_Idx;
    from_file(const string& path, bool uniq = true) 
    : m_Uniq(uniq), m_Idx(0)
    {
        ifstream file(path);
        string line;
        while (getline(file, line))
        {
            m_Data.emplace_back(line);
        }
        if (m_Data.empty())
            throw invalid_argument("Nenalezen file: "s + quote(path));
    }

    string operator()()
    {
        if (m_Uniq)
        {
            m_Idx++;
            m_Idx %= m_Data.size();
            return quote(m_Data[m_Idx]);
        }
        return quote(random_from(m_Data));
    }
};

class sequence
{
public:
    int m_Start;
    int m_Max;
    bool m_Mod;
    int m_Idx;

    sequence(int start = 1, int max = INT_MAX, bool mod = false)
    : m_Start(start), m_Max(max), m_Mod(mod), m_Idx(m_Start)
    {}

    string operator()()
    {
        if (m_Idx == m_Max)
        {
            if (!m_Mod) throw out_of_range("Sequence prekrocila max hodnotu. Muzes nastavit mod = true, potom se bude opakovat od 1.");
            m_Idx = m_Start;
        }

        ostringstream stream;
        stream << m_Idx++;
        return stream.str();
    }
};


class random_int
{
public:
    int m_Min;
    int m_Max;
    random_int(int min, int max)
    {
        m_Min = min;
        m_Max = max;
    }

    string operator()() const
    {
        ostringstream stream;
        int number = rand() % (m_Max - m_Min) + m_Min;
        stream << number;
        return stream.str();
    }
};

class date
{
public:
    long long m_Year_from;
    long long m_Year_to;
    date(long long year_from, long long year_to):
        m_Year_from(year_from), m_Year_to(year_to) {}

    string operator()()
    {
        ostringstream stream;
        stream << setfill('0');
        stream << setw(4) << random_int(m_Year_from, m_Year_to)();
        stream << "-";
        stream << setw(2) << random_int(1, 13)();
        stream << "-";
        stream << setw(2) << random_int(1, 29)();
        return quote(stream.str());
    }
};

string TODO()
{
    return "TODO";
}

string null()
{
    return "null";
}

class Field
{
public:
    string m_Name;
    function<string()> m_Generator;
    int m_Blank_probability;
    Field(string name, function<string()> generator, int blank_probability)
        : m_Name(name), m_Generator(generator), m_Blank_probability(blank_probability)
    {}

    string value()
    {
        int chance = rand() % 100;
        if (chance >= m_Blank_probability) return null();
        return m_Generator();
    }
};

void insert(const string& table_name, size_t count, initializer_list<Field>fieldsIn)
{
    list<Field>fields;
    move(fieldsIn.begin(), fieldsIn.end(), back_inserter(fields));

    RESET_SEQ = true;
    for (size_t i = 0; i < count; i++)
    {
        ostringstream stream;
        stream << "insert into " << table_name << " " << "(";

        stream << fields.begin()->m_Name;
        for (auto it = ++fields.begin(); it != fields.end(); ++it)
            stream << ", " << it->m_Name;

        stream << ") values (";

        stream << (fields.begin())->value();
        for (auto it = ++fields.begin(); it != fields.end(); ++it)
            stream << ", " << it->value();
        stream << ");";
        cout << stream.str() << endl;
    }
    cout << endl << endl;
}


string DEFAULT()
{
    return "DEFAULT";
}
