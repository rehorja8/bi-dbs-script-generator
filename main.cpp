#include "generator.cpp"


int main()
{

    #define ZAKAZNIK_CNT 50
    insert("zakaznik", ZAKAZNIK_CNT, {
            Field("id_zakaznik", DEFAULT, 100),
            Field("jmeno", alien_name, 100),
            Field("rasa", alien_race, 80)
        });

    #define GARAZ_CNT 30
    insert("garaz", GARAZ_CNT, {
            Field("id_garaz", DEFAULT, 100),
            Field("kapacita", random_int(5, 20), 100)
        });

    #define OPRAVNA_CNT 20
    insert("opravna", OPRAVNA_CNT, {
            Field("id_opravna", DEFAULT, 100),
            Field("kapacita", random_int(1, 10), 100)
        });

    #define LOD_CNT 60
    #define LOD_V_G_CNT 15
    // lode do garaze
    insert("vesmirna_lod", LOD_V_G_CNT, {
            Field("id_vesmirna_lod", DEFAULT, 100),
            Field("id_garaz", random_int(1, 30), 40),
            Field("id_opravna", random_int(1, 50), 0),
            Field("id_zakaznik", random_int(1, 50), 100),
            Field("znacka_lodi", from_file("space_ships"), 90),
            Field("typ_pohonu", from_file("engines"), 90)
        });

    // lode do opravny
    insert("vesmirna_lod", LOD_CNT - LOD_V_G_CNT, {
            Field("id_vesmirna_lod", DEFAULT, 100),
            Field("id_garaz", random_int(1, 30), 0),
            Field("id_opravna", random_int(1, 50), 80),
            Field("id_zakaznik", random_int(1, 50), 100),
            Field("znacka_lodi", from_file("space_ships"), 90),
            Field("typ_pohonu", from_file("engines"), 90)
        });

    #define ZAKAZKA_CNT 100
    insert("zakazka", ZAKAZKA_CNT, {
            Field("id_zakazka", DEFAULT, 100),
            Field("id_vesmirna_lod", random_int(1, LOD_CNT), 100),
            Field("datum_prijeti", date(40'000, 40'005), 100),
            Field("datum_navraceni", date(40'005, 60'000), 60),
            Field("popis_obsluhy", from_file("repairs"), 80),
            Field("cena_zakazky", random_int(30'000, 1'000'000'000), 90),
            Field("zaplaceno", boolean, 100)
        });

    #define POMUCKA_CNT 20
    insert("pomucka", POMUCKA_CNT, {
            Field("id_pomucka", DEFAULT, 100),
            Field("nazev", from_file("space_tools"), 100),
        });


    #define ZAMESTNANEC_ZAKAZKA_CNT 100
    insert("zamestnanec_zakazka", ZAMESTNANEC_ZAKAZKA_CNT, {
            Field("id_zamestnanec", TODO, 100),
            Field("id_zakazka", TODO, 100),
        });
    
    insert("seq test", 100, {
            Field("cislo", from_file("seq", true), 100),
            Field("cislo", sequence(1, 5, true), 100)
        });



}
